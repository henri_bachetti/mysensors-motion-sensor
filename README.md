# MYSENSORS BATTERY MOTION SENSOR

The purpose of this page is to explain step by step the realization of a low power motion sensor based on ARDUINO PRO MINI, connected by radio to a DOMOTICZ server, using an NRF24L01 2.4GHZ module.

The board uses the following components :

 * an ARDUINO PRO MINI 3.3V 8MHz
 * a NRF24L01
 * an HC-SR501 motion sensor
 * a LM2936-3.3 regulator
 * some passive components
 * the board is powered by a 200mAH LITHIUM-ION battery.

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.5.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2018/04/un-detecteur-de-mouvement-mysensors-sur.html

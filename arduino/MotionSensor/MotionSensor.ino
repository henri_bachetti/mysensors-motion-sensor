/**
 * The MySensors Arduino library handles the wireless radio link and protocol
 * between your home built sensors/actuators and HA controller of choice.
 * The sensors forms a self healing radio network with optional repeaters. Each
 * repeater and gateway builds a routing tables in EEPROM which keeps track of the
 * network topology allowing messages to be routed to nodes.
 *
 * Created by Henrik Ekblad <henrik.ekblad@mysensors.org>
 * Copyright (C) 2013-2015 Sensnology AB
 * Full contributor list: https://github.com/mysensors/Arduino/graphs/contributors
 *
 * Documentation: http://www.mysensors.org
 * Support Forum: http://forum.mysensors.org
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 *******************************
 *
 * REVISION HISTORY
 * Version 1.0 - Henrik Ekblad
 *
 * DESCRIPTION
 * Motion Sensor example using HC-SR501
 * http://www.mysensors.org/build/motion
 *
 */

// Enable debug prints
//#define MY_DEBUG

// Enable and select radio type attached
#define MY_RADIO_NRF24
//#define MY_RADIO_NRF5_ESB
//#define MY_RADIO_RFM69
//#define MY_RADIO_RFM95

#define MY_RF24_CE_PIN    7
#define MY_RF24_CS_PIN    8

#include <MySensors.h>

uint32_t SLEEP_TIME = 120000; // Sleep time between reports (in milliseconds)
#define DIGITAL_INPUT_SENSOR  3   // The digital input you attached your motion sensor.  (Only 2 and 3 generates interrupt!)
#define LED                   10
#define CHILD_ID              1   // Id of the sensor child
#define VREF                  1.099

struct batteryCapacity
{
  float voltage;
  int capacity;
};

const batteryCapacity remainingCapacity[] = {
  4.20,   100,
  4.10,   96,
  4.00,   92,
  3.96,   89,
  3.92,   85,
  3.89,   81,
  3.86,   77,
  3.83,   73,
  3.80,   69,
  3.77,   65,
  3.75,   62,
  3.72,   58,
  3.70,   55,
  3.66,   51,
  3.62,   47,
  3.58,   43,
  3.55,   40,
  3.51,   35,
  3.48,   32,
  3.44,   26,
  3.40,   24,
  3.37,   20,
  3.35,   17,
  3.27,   13,
  3.20,   9,
  3.1,    6,
  3.00,   3,
};

const int ncell = sizeof(remainingCapacity) / sizeof(struct batteryCapacity);

// Initialize motion message
MyMessage msg(CHILD_ID, V_TRIPPED);

unsigned int getBatteryCapacity(void)
{
  analogReference(INTERNAL);
  analogRead(0);
  delay(1);
  unsigned int adc = analogRead(0);
#ifdef MY_DEBUG
  Serial.print("ADC: ");
  Serial.println(adc);
#endif
  float voltage = adc * VREF / 1023 / 0.244;
#ifdef MY_DEBUG
  Serial.print("VCC: ");
  Serial.println(voltage, 3);
#endif
for (int i = 0 ; i < ncell ; i++){
#ifdef MY_DEBUG
    Serial.print(i);
    Serial.print(" : ");
    Serial.print(remainingCapacity[i].voltage);
    Serial.print(" | ");
    Serial.println(remainingCapacity[i].capacity);
#endif
    if (voltage > remainingCapacity[i].voltage) {
      return remainingCapacity[i].capacity;
    }
  }
  return 0;
}

void setup()
{
  pinMode(DIGITAL_INPUT_SENSOR, INPUT);
  pinMode(LED, OUTPUT);
}

void presentation()
{
  Serial.print("MYSENSORS MOTION SENSOR: ");
  // Send the sketch version information to the gateway and Controller
  sendSketchInfo("Motion Sensor", "1.0");

  // Register all sensors to gw (they will be created as child devices)
  present(CHILD_ID, S_MOTION);
  Serial.println("OK");
}

void loop()
{
  // Read digital motion value
  bool tripped = digitalRead(DIGITAL_INPUT_SENSOR) == HIGH;

  int batteryLevel = getBatteryCapacity();
  sendBatteryLevel(batteryLevel);
  Serial.print("transmitted battery level OK: ");
  Serial.print(batteryLevel);
  Serial.println("%");
  Serial.print("Motion detected: ");
  Serial.println(tripped);
  digitalWrite(LED, tripped ? LOW : HIGH);
  send(msg.set(tripped ? "1" : "0"));  // Send tripped value to gw

  // Sleep until interrupt comes in on motion sensor. Send update every two minute.
  sleep(digitalPinToInterrupt(DIGITAL_INPUT_SENSOR), CHANGE, SLEEP_TIME);
}



